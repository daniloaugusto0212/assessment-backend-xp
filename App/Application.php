<?php

namespace App;

use App\Controllers\LoginController;

class Application
{
    private $controller;
    private $function;

    /**
     * Executas as rotas e chama o controlador e seus métodos
     *
     * @return void
     */
    public function setApp()
    {
        if (!isset($_SESSION['login'])) {
            $this->controller = new LoginController();
        } else {
            $loadName = 'App\Controllers\\';
            $url = explode('/', @$_GET['url']);

            if (empty($url[0])) {
                $loadName .= 'Home';
            } else {
                $loadName .= ucfirst(strtolower($url[0]));
            }

            $loadName .= 'Controller';

            if (isset($url[1])) {
                $loadName .= '@' . $url[1];
                if (isset($url[2])) {
                    $loadName .= '_' . $url[2];
                    $idSeparate = explode('_', $loadName);
                    $loadName = $idSeparate[0];
                    $id = $idSeparate[1];
                }
            }

            $verifyFunction = explode('@', $loadName);
            if (isset($verifyFunction[1])) {
                $this->function = $verifyFunction[1];
                if (isset($url[2])) {
                    $this->function = $verifyFunction[1] . '_' . $id;
                }
            } else {
                $this->function = '';
            }

            $loadName = $verifyFunction[0];

            if (file_exists($loadName . '.php')) {
                $this->controller = new $loadName();
            } else {
                include('Views/pages/404.php');
                include('Views/partials/footer.php');
                die();
            }
        }
    }

    /**
     * Roda a aplicação
     *
     * @return void
     */
    public function run()
    {
        $this->setApp();
        $idSeparate = explode('_', $this->function);
        if (!empty($idSeparate[0])) {
            $method = $idSeparate[0];
            if (!empty($_POST)) {
                $method .= 'Post';
            }
        } else {
            $method = $this->function;
        }
        $id = isset($idSeparate[1]) ? $idSeparate[1] : '';
        if (!empty($id)) {
            $this->controller->$method($id);
        } elseif (!empty($method)) {
            $this->controller->$method();
        } else {
            $this->controller->index();
        }
    }
}
