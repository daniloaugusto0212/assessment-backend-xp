<?php

use App\Models\Product;
use App\Models\Category;
use App\Controllers\classes\Utils;

$products = new Product();
$product = $products->find(ID);

$cat = new Category();
$categoryList = $cat->show();

$catProduct = $products->showCategories(ID);
$catProductName = [];
foreach ($catProduct as $cat) {
    $catProductName[] = $cat['name'];
}

?>
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Editar Produto</h1>
    
    <form method="post" enctype="multipart/form-data">
    <div class="input-field">
        <label for="sku" class="label">SKU</label>
        <input type="text" id="sku" name="sku" class="input-text" value="<?= $product['sku'] ?>"/>
      </div>
      <div class="input-field">
        <label for="name" class="label">Nome</label>
        <input type="text" id="name" name="name" class="input-text" value="<?= $product['name'] ?>"/>
      </div>
      <div class="input-field">
        <label for="price" class="label">Preço</label>
        <input type="text" id="price" name="price" class="input-text" value="<?= Utils::convertMoney($product['price']) ?>"/> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantidade</label>
        <input type="text" id="quantity" name="quantity" class="input-text" value="<?= $product['quantity'] ?>"/> 
      </div>
      <div class="input-field">
        <label for="category" class="label">Categoria</label>
        <select multiple id="category" name="category_id[]" class="input-text">

          <?php foreach ($catProduct as $selecteds) : ?>
            <option value="<?= $selecteds['category_id'] ?>" selected="selected"><?= $selecteds['name'] ?></option>
          <?php endforeach ?>
          
          <?php foreach ($categoryList as $categoria) {
                if (in_array($categoria['name'], $catProductName)) {
                    continue;
                } ?>
            <option value="<?= $categoria['id'] ?>"><?= $categoria['name'] ?></option>
          <?php } ?>
                
        </select>
      </div>
      <div class="input-field">
        <label for="description" class="label">Descrição</label>
        <textarea id="description" name="description" class="input-text"><?= $product['description'] ?></textarea>
      </div>

      <div class="input-field">
        <label for="image" class="label">Alterar Imagem</label>
        <input type="file" accept=".jpg, .png, .jpeg" id="image" name="image" class="input-text" value="<?= $product['image'] ?>"/> 
      </div>
      <div class="input-field">
        <img src="<?= PATH ?>/uploads/<?= $product['image'] ?>" style="max-width: 60px" alt="Imagem do produto">
        <input type="hidden" name="current_image" value="<?= $product['image'] ?>">
      </div>
      <div class="actions-form">
        <a href="<?= PATH ?>/produto" class="action back">Voltar</a>
        <input class="btn-submit btn-action"  type="submit" value="Atualizar" />
      </div>
    </form>
  </main>
  <!-- Main Content -->
