<?php

use App\Models\Category;

$categorias = new Category();
$categoryList = $categorias->show();

?>
  <!-- Main Content -->
  <main class="content" >
    <h1 class="title new-item">Novo Produto</h1>
    
    <form method="post" enctype="multipart/form-data">
      <div class="input-field">
        <label for="sku" class="label">SKU</label>
        <input type="text" id="sku" name="sku" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="name" class="label">Nome</label>
        <input type="text" id="name" name="name" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="price" class="label">Preço</label>
        <input type="text" id="price" name="price" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantidade</label>
        <input type="text" id="quantity" name="quantity" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="category" class="label">Categoria</label>
        <select multiple id="category" name="category_id[]" class="input-text">

          <?php foreach ($categoryList as $categoria) : ?>
            <option value="<?= $categoria['id'] ?>"><?= $categoria['name'] ?></option>
          <?php endforeach ?>
        </select>
      </div>
      <div class="input-field">
        <label for="description" class="label">Descrição</label>
        <textarea id="description" name="description" class="input-text"></textarea>
      </div>
      <div class="input-field">
        <label for="image" class="label">Imagem</label>
        <input type="file" accept=".jpg, .png, .jpeg" id="image" name="image" class="input-text" /> 
      </div>
      <div class="actions-form">
        <a href="<?= PATH ?>/produto" class="action back">Voltar</a>
        <input class="btn-submit btn-action" type="submit" value="Salvar" />
      </div>
      
    </form>
  </main>
  <!-- Main Content -->
