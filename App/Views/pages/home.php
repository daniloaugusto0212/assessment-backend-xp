<?php

use App\Controllers\classes\Utils;
use App\Models\Product;

$products = new Product();

$productList = $products->show();

?>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Dashboard</h1>
    </div>
    <div class="infor">
      Existem 4 produtos cadastrados na loja: <a href="<?= PATH ?>/produto/new" class="btn-action">Novo Produto</a>
    </div>
    <ul class="product-list">
      <?php foreach ($productList as $product) : ?>
      <li>
        <div class="product-image">
          <a href="<?= PATH ?>/produto/edit/<?= $product['id'] ?>" title="<?= $product['name'] ?>">
            <img src="<?= PATH ?>/uploads/<?= $product['image'] ?>" layout="responsive" width="164" height="145" alt="<?= $product['name'] ?>" />
          </a>
        </div>
        <div class="product-info">
          <div class="product-name"><span><?= $product['name'] ?></span></div>
          <div class="product-price"><span class="special-price">1 available</span> <span>R$<?= Utils::convertMoney($product['price']) ?></span></div>
        </div>
      </li>
      <?php endforeach ?>
    </ul>
  </main>
  <!-- Main Content -->

