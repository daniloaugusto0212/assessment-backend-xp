
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Cadastro de Usuário</h1>
    
    <form id="register" method="post">
      <div class="input-field">
        <label for="name" class="label">Nome</label>
        <input type="text" id="name" name="name" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="email" class="label">E-mail</label>
        <input type="text" id="email" name="email" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="password" class="label">Senha</label>
        <input type="password" id="password" name="password" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="passConfirm" class="label">Confirmação de Senha</label>
        <input type="password" id="passConfirm" name="passConfirm" class="input-text" /> 
      </div>
      <div class="actions-form">
        <a href="<?= PATH ?>/usuario" class="action back">Voltar</a>
        <input class="btn-submit btn-action" type="submit" name="action" value="Cadastrar" />
      </div>
    </form>
  </main>
  <!-- Main Content -->
