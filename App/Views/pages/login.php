
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Login</h1>
    <form method="post" action="<?= PATH ?>/login">
      <div class="input-field">
        <label for="email" class="label">E-mail</label>
        <input type="text" id="email" name="email" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="password" class="label">Senha</label>
        <input type="password" id="password" name="password" class="input-text" /> 
      </div>
      <div class="actions-form">
        <input class="btn-submit btn-action" type="submit" name="login" value="Logar" />
      </div>
    </form>
  </main>
  <!-- Main Content -->
