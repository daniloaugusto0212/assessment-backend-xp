<?php

use App\Models\Category;

$categorias = new Category();
$category = $categorias->find(ID);

?>
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Editar Categoria</h1>
    
    <form method="post">
      <div class="input-field">
        <label for="name" class="label">Nome</label>
        <input type="text" id="name" name="name" class="input-text" value="<?= $category['name'] ?>"/>
      </div>
      <div class="input-field">
        <label for="code" class="label">Código</label>
        <input type="text" id="code" name="code" class="input-text" value="<?= $category['code'] ?>"/>
      </div>
      <div class="actions-form">
        <a href="<?= PATH ?>/categoria" class="action back">Voltar</a>
        <input class="btn-submit btn-action"  type="submit" value="Atualizar" />
      </div>
    </form>
  </main>
  <!-- Main Content -->
