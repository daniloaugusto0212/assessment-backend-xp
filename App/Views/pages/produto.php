<?php

use App\Controllers\classes\Utils;
use App\Models\Product;

$products = new Product();
$productList = $products->show();
$categories = $products->showCategories();

?>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Produtos</h1>
      <a href="<?= PATH ?>/produto/new" class="btn-action">Novo Produto</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Nome</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">SKU</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Preço</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Quantidade</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categorias</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Imagem</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Ações</span>
        </th>
      </tr>
      <?php foreach ($productList as $product) : ?>
      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $product['name'] ?></span>
        </td>
      
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $product['sku'] ?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content">R$ <?= Utils::convertMoney($product['price']) ?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $product['quantity'] ?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content">
            <?php foreach ($categories as $cat) : ?>
                <?php if ($product['id'] == $cat['id']) : ?>
                    <?= $cat['name'] ?><Br />
                <?php endif ?>
            <?php endforeach ?>
            </span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content">
             <img src="<?= PATH ?>/uploads/<?= $product['image'] ?>" style="max-width: 60px" alt="Imagem do produto">
           </span>
        </td>
      
        <td class="data-grid-td">
        <div class="actions">
            <div class="action edit"><span><a href="<?= PATH ?>/produto/edit/<?= $product['id'] ?>">Editar</a> </span></div>
            <div class="action delete"><span><a href="<?= PATH ?>/produto/delete/<?= $product['id'] ?>">Excluir</a></span></div>
          </div>
        </td>
      </tr>
      <?php endforeach ?>
    </table>
  </main>
  <!-- Main Content -->
