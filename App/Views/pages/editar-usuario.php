<?php

use App\Models\User;

$users = new User();
$user = $users->find(ID);

?>
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Editar Usuário</h1>
    
    <form method="post">
      <div class="input-field">
        <label for="name" class="label">Nome</label>
        <input type="text" id="name" name="name" class="input-text" value="<?= $user['name'] ?>"/>
      </div>
      <div class="input-field">
        <label for="email" class="label">E-mail</label>
        <input type="text" id="email" name="email" class="input-text" value="<?= $user['email'] ?>"/>
      </div>
      <small>Para alterar a senha, <a href="<?= PATH ?>/usuario/delete/<?= $user['id'] ?>">exclua</a>  o usuárrio e cadastre novamente.</small>

      <div class="actions-form">
        <a href="<?= PATH ?>/usuario" class="action back">Voltar</a>
        <input class="btn-submit btn-action"  type="submit" value="Atualizar" />
      </div>
    </form>
  </main>
  <!-- Main Content -->
