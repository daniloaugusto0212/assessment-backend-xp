<?php

use App\Models\User;

$users = new User();
$userList = $users->show();

?>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Usuários</h1>
      <a href="<?= PATH ?>/usuario/new" class="btn-action">Novo Usuário</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Nome</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">E-mail/Usuário</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Ações</span>
        </th>
      </tr>
      <?php foreach ($userList as $product) : ?>
      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $product['name'] ?></span>
        </td>
      
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $product['email'] ?></span>
        </td>

        
      
        <td class="data-grid-td">
        <div class="actions">
            <div class="action edit"><span><a href="<?= PATH ?>/usuario/edit/<?= $product['id'] ?>">Editar</a> </span></div>
            <div class="action delete"><span><a href="<?= PATH ?>/usuario/delete/<?= $product['id'] ?>">Excluir</a></span></div>
          </div>
        </td>
      </tr>
      <?php endforeach ?>
    </table>
  </main>
  <!-- Main Content -->
