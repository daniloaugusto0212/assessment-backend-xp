<?php

use App\Models\Category;

$categorias = new Category();
$list = $categorias->show();

?>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Categorias</h1>
      <a href="<?= PATH ?>/categoria/new" class="btn-action">Nova Categoria</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Código</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Ações</span>
        </th>
      </tr>
      
      <?php foreach ($list as $category) : ?>
      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $category['name'] ?></span>
        </td>
      
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?= $category['code'] ?></span>
        </td>
      
        <td class="data-grid-td">
          <div class="actions">
            <div class="action edit"><span><a href="<?= PATH ?>/categoria/edit/<?= $category['id'] ?>">Editar</a> </span></div>
            <div class="action delete"><span><a href="<?= PATH ?>/categoria/delete/<?= $category['id'] ?>">Excluir</a></span></div>
          </div>
        </td>
      </tr>
      
      <?php endforeach ?>
    </table>
  </main>
  <!-- Main Content -->
