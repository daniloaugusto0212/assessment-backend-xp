<?php

namespace App\Controllers;

use App\Views\MainView;

class HomeController
{
    /**
     * Renderiza a página principal
     *
     *
     */
    public function index()
    {
        MainView::render('home');
    }
}
