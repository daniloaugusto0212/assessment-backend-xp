<?php

namespace App\Controllers;

use App\Controllers\classes\Utils;
use App\Models\User;
use App\Views\MainView;
use Bcrypt\Bcrypt;

class UsuarioController
{
    /**
     * Renderiza a página principal de usuários
     *
     *
     */
    public function index()
    {
        MainView::render('usuario');
    }

    /**
     * Renderiza página de inserção de usuários
     *
     *
     */
    public function new()
    {
        MainView::render('novo-usuario');
    }

    /**
     * Recebe os dados do formulário de usuários, valida e cadastra
     *
     *
     */
    public function newPost()
    {
        $data = $_POST;
        if (empty($data['name'])) {
            Utils::alert('O nome não pode ser vazio!');
            Utils::redirect(PATH . '/usuario');
        } elseif (empty($data['email'])) {
            Utils::alert('O E-mail não pode ser vazio!');
            Utils::redirect(PATH . '/usuario');
        } elseif ($data['password'] != $data['passConfirm']) {
            Utils::alert('As senhas não conferem!');
            Utils::redirect(PATH . '/usuario');
        } elseif (strlen($data['password']) < 6) {
            Utils::alert('A senha precisa ter 6 caracteres.');
            Utils::redirect(PATH . '/usuario');
        } elseif (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            Utils::alert('E-mail inválido!');
            Utils::redirect(PATH . '/usuario');
        } else {
            $user = new User();
            $user->name = $data['name'];
            $user->email = $data['email'];
            $bcrypt = new Bcrypt();
            $bcrypt_version = '2a';
            $user->password = $bcrypt->encrypt($data['password'], $bcrypt_version);
            if ($user->save()) {
                Utils::alert('Cadastro efetuado com sucesso!');
                Utils::redirect(PATH . '/usuario');
            } else {
                Utils::alert('Já existe um usuário com este E-mail!');
                Utils::redirect(PATH . '/usuario');
            }
        }
    }

    /**
     * Renderiza página de edição de usuários
     *
     * @param int $id
     *
     */
    public function edit($id = null)
    {
        if ($id == null) {
            Utils::alert('Você precisa passar o parâmetro ID!');
            Utils::redirect(PATH . '/usuario');
        } else {
            MainView::render('editar-usuario');
        }
    }

     /**
     * Recebe os dados da edição de usuários, valida e atualiza
     *
     * @param int $id
     *
     */
    public function editPost($id)
    {
        $user = new User();
        $data = $_POST;

        if (empty($data['name'])) {
            Utils::alert('O nome não pode ser vazio!');
            Utils::redirect(PATH . '/usuario/edit/' . $id);
        } elseif (empty($data['email'])) {
            Utils::alert('O E-mail não pode ser vazio!');
            Utils::redirect(PATH . '/usuario/edit/' . $id);
        } else {
            $user->name = $data['name'];
            $user->email = $data['email'];
            if ($user->update($id)) {
                Utils::alert('Usuário atualizado com sucesso!');
            } else {
                Utils::alert('Usuário não atualizado!');
            }

            Utils::redirect(PATH . '/usuario');
        }
    }

    /**
     * Exclui o usuário passado como parâmetro
     *
     * @param int $id
     *
     */
    public function delete($id)
    {
        $user = new User();

        if ($user->delete($id)) {
            Utils::alert('Usuário excluído com sucesso!');
        } else {
            Utils::alert('Houve um erro ao tentar excluir!');
        }
        Utils::redirect(PATH . '/usuario');
    }
}
