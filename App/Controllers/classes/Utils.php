<?php

namespace App\Controllers\classes;

class Utils
{
    /**
     * Redireciona para a URL passada como parâmetro
     *
     * @param string $url
     * @return void
     */
    public static function redirect($url)
    {
        echo '<script>window.location.href="' . $url . '"</script>';
        die();
    }

    /**
     * Mostra um alerta padrão em javascript
     *
     * @param string $msg
     * @return void
     */
    public static function alert($msg)
    {
        echo '<script>alert("' . $msg . '")</script>';
    }

    /**
     * Destrói a sessão atual
     *
     * @return void
     */
    public static function logout()
    {
        unset($_SESSION['login']);
        session_destroy();
        self::redirect(PATH . '/login');
    }

    /**
     * Valida o formato da imagem jpeg/jpg/png
     *
     * @param file $image
     * @return void
     */
    public static function imageValidate($image)
    {
        if (
            $image['type'] == 'image/jpeg' ||
            $image['type'] == 'image/jpg' ||
            $image['type'] == 'image/png'
        ) {
            $size = intval($image['size'] / 1024);
            if ($size < 900) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Faz o upload da imagem na pasta UPLOADS
     *
     * @param file $file
     * @return void
     */
    public static function uploadFile($file)
    {
        $formatFile = explode('.', $file['name']);
        $fileName = uniqid() . '.' . $formatFile[count($formatFile) - 1];
        if (move_uploaded_file($file['tmp_name'], DIR_UPLOAD . $fileName)) {
            return $fileName;
        } else {
            return false;
        }
    }

    /**
     * Exclui a imagem da pasta UPLOADS
     *
     * @param file $file
     * @return void
     */
    public static function deleteFile($file)
    {
        @unlink(DIR_UPLOAD . $file);
    }

    /**
     * Formata o preço antes de inserir no Banco de Dados
     *
     * @param string $value
     * @return void
     */
    public static function formatCurrencyDb($value)
    {
        $value = str_replace('.', '', $value);
        $value = str_replace(',', '.', $value);
        return $value;
    }

    /**
     * Converte o preço para valor BRL
     *
     * @param float $value
     * @return void
     */
    public static function convertMoney($value)
    {
        return number_format($value, 2, ',', '.');
    }
}
