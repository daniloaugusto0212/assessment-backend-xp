<?php

namespace App\Controllers;

use App\Models\Product;
use App\Views\MainView;
use App\Controllers\classes\Utils;

class ProdutoController
{
    /**
     * Renderiza a página principal dos produtos
     *
     *
     */
    public function index()
    {
        MainView::render('produto');
    }

    /**
     * Recebe os dados do formulário de produtos, valida e cadastra
     *
     *
     */
    public function newPost()
    {
        $product = new Product();
        $data = $_POST;
        $image = $_FILES['image'];

        if (empty($data['sku'])) {
            Utils::alert('O SKU não pode ser vazio!');
            Utils::redirect(PATH . '/produto/new');
        } elseif (empty($data['name'])) {
            Utils::alert('O Nome não pode ser vazio!');
            Utils::redirect(PATH . '/produto/new');
        } elseif (empty($data['price'])) {
            Utils::alert('O Preço não pode ser vazio!');
            Utils::redirect(PATH . '/produto/new');
        } elseif (!isset($data['category_id'])) {
            Utils::alert('A categoria não pode ser vazia!');
            Utils::redirect(PATH . '/produto/new');
        } elseif (empty($image['name'])) {
            Utils::alert('Selecione uma imagem.');
            Utils::redirect(PATH . '/produto/new');
        } elseif (!Utils::imageValidate($image)) {
            Utils::alert('Formato da imagem inválido!');
            Utils::redirect(PATH . '/produto/new');
        } else {
            $product->category_id = $data['category_id'];
            $product->sku = $data['sku'];
            $product->name = $data['name'];
            $product->price = Utils::formatCurrencyDb($data['price']);
            $product->quantity = $data['quantity'];
            $product->description = $data['description'];
            $product->image = Utils::uploadFile($image);
            if ($product->save()) {
                Utils::alert('Cadastro efetuado com sucesso!');
            } else {
                Utils::alert('Já existe um Produto com este Nome!');
            }

            Utils::redirect(PATH . '/produto/new');
        }
    }

    /**
     * Renderiza página de inserção de produtos
     *
     *
     */
    public function new()
    {
        MainView::render('novo-produto');
    }

    /**
     * Renderiza página de edição de produtos
     *
     * @param int $id
     *
     */
    public function edit($id = null)
    {
        if ($id == null) {
            Utils::alert('Você precisa passar o parâmetro ID!');
            Utils::redirect(PATH . '/produto');
        } else {
            MainView::render('editar-produto');
        }
    }

     /**
     * Recebe os dados da edição de produtos, valida e atualiza
     *
     * @param int $id
     *
     */
    public function editPost($id)
    {
        $product = new Product();
        $data = $_POST;
        $image = $_FILES['image'];

        if (empty($data['sku'])) {
            Utils::alert('O SKU não pode ser vazio!');
            Utils::redirect(PATH . '/produto/edit/' . $id);
        } elseif (empty($data['name'])) {
            Utils::alert('O Nome não pode ser vazio!');
            Utils::redirect(PATH . '/produto/edit/' . $id);
        } elseif (empty($data['price'])) {
            Utils::alert('O Preço não pode ser vazio!');
            Utils::redirect(PATH . '/produto/edit/' . $id);
        } elseif (!isset($data['category_id'])) {
            Utils::alert('A categoria não pode ser vazia!');
            Utils::redirect(PATH . '/produto/edit/' . $id);
        } elseif (empty($image['name'])) {
            $product->image = $data['current_image'];
        } elseif (!Utils::imageValidate($image)) {
            Utils::alert('Formato da imagem inválido!');
            Utils::redirect(PATH . '/produto/edit/' . $id);
        } else {
            Utils::deleteFile($data['current_image']);
            $product->image = Utils::uploadFile($image);
        }
        $product->category_id = $data['category_id'];
        $product->sku = $data['sku'];
        $product->name = $data['name'];
        $product->price = Utils::formatCurrencyDb($data['price']);
        $product->quantity = $data['quantity'];
        $product->description = $data['description'];
        if ($product->update($id)) {
            Utils::alert('Produto atualizado com sucesso!');
        } else {
            Utils::alert('Produto não atualizado!');
        }

        Utils::redirect(PATH . '/produto');
    }

    /**
     * Exclui o produto passado como parâmetro
     *
     * @param int $id
     *
     */
    public function delete($id)
    {
        $product = new Product();

        if ($product->delete($id)) {
            Utils::alert('Produto excluído com sucesso!');
        } else {
            Utils::alert('Houve um erro ao tentar excluir!');
        }
        Utils::redirect(PATH . '/produto');
    }
}
