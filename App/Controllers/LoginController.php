<?php

namespace App\Controllers;

use App\Controllers\classes\Utils;
use App\Models\Login;
use App\Views\MainView;

class LoginController
{
    /**
     * Recebe os dados do formulário de login e os envia para a Model;
     * Verifica se existe sessão ativa
     *
     *
     */
    public function index()
    {
        if (isset($_POST['login'])) {
            if (Login::login($_POST['email'], $_POST['password'])) {
                Utils::redirect(PATH . '/home');
            } else {
                Utils::alert('Usuário ou senha inválidos');
                Utils::redirect(PATH . '/login');
            }
        } elseif (isset($_SESSION['login'])) {
            Utils::redirect(PATH . '/home');
        } else {
            MainView::render('login');
        }
    }
}
