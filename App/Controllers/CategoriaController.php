<?php

namespace App\Controllers;

use App\Controllers\classes\Utils;
use App\Models\Category;
use App\Views\MainView;

class CategoriaController
{
    /**
     * Renderiza página principal das categorias
     *
     *
     */
    public function index()
    {
        MainView::render('categoria');
    }

    /**
     * Renderiza página de inserção de categorias
     *
     *
     */
    public function new()
    {
        MainView::render('nova-categoria');
    }

    /**
     * Recebe os dados do formulário, valida e cadastra
     *
     *
     */
    public function newPost()
    {
        $category = new Category();
        $data = $_POST;

        if (empty($data['name'])) {
            Utils::alert('O nome não pode ser vazio!');
            Utils::redirect(PATH . '/categoria/new');
        } elseif (empty($data['code'])) {
            Utils::alert('O Código não pode ser vazio!');
            Utils::redirect(PATH . '/categoria/new');
        } else {
                $category->name = $data['name'];
                $category->code = $data['code'];
            if ($category->save()) {
                Utils::alert('Cadastro efetuado com sucesso!');
            } else {
                Utils::alert('Já existe uma categoria com este Nome!');
            }

            Utils::redirect(PATH . '/categoria/new');
        }
    }

    /**
     * Renderiza página de edição de categorias
     *
     * @param int $id
     *
     */
    public function edit($id = null)
    {
        if ($id == null) {
            Utils::alert('Você precisa passar o parâmetro ID!');
            Utils::redirect(PATH . '/categoria');
        } else {
            MainView::render('editar-categoria');
        }
    }

    /**
     * Recebe os dados da edição de categorias, valida e atualiza
     *
     * @param int $id
     *
     */
    public function editPost($id)
    {
        $category = new Category();
        $data = $_POST;

        if (empty($data['name'])) {
            Utils::alert('O nome não pode ser vazio!');
            Utils::redirect(PATH . '/categoria/edit/' . $id);
        } elseif (empty($data['code'])) {
            Utils::alert('O Código não pode ser vazio!');
            Utils::redirect(PATH . '/categoria/edit/' . $id);
        } else {
            $category->name = $data['name'];
            $category->code = $data['code'];
            if ($category->update($id)) {
                Utils::alert('Categoria atualizada com sucesso!');
            } else {
                Utils::alert('Categoria não atualizado!');
            }

            Utils::redirect(PATH . '/categoria');
        }
    }

    /**
     * Exclui a categoria passado como parâmetro
     *
     * @param int $id
     *
     */
    public function delete($id)
    {
        $category = new Category();

        if ($category->delete($id)) {
            Utils::alert('Categoria excluída com sucesso!');
        } else {
            Utils::alert('Houve um erro ao tentar excluir! Existem produtos vinculados a esta categoria.');
        }
        Utils::redirect(PATH . '/categoria');
    }
}
