<?php

namespace App\Models;

class Category extends Manager
{
    public $name;
    public $code;
    public static $table = 'tb_categories';
    public static $tableIntegration = 'tb_product_category';
    public static $parmVerify = 'name';

    /**
     * Salva Categoria no banco de dados
     *
     *
     */
    public function save()
    {
        $data = array('name' => $this->name, 'code' => $this->code);

        $exists = $this->verifyExists(self::$table, self::$parmVerify, $data[self::$parmVerify]);
        if ($exists != null) {
            return false;
        } else {
            $this->insert(self::$table, $data);
            return true;
        }
    }

    /**
     * Retorna a lista das categorias
     *
     *
     */
    public function show()
    {
        return $this->list(self::$table);
    }

    /**
     * Busca a categoria passada como parâmetro
     *
     * @param int $id
     *
     */
    public function find($id)
    {
        return $this->getInfo(self::$table, $id);
    }

    /**
     * Atualiza a categoria passada como parâmetro
     *
     * @param int $id
     * @return boolean
     */
    public function update($id)
    {
        $data = array('name' => $this->name, 'code' => $this->code);

        $this->updateM(self::$table, $data, $id);
        return true;
    }

    /**
     * Exclui a categoria passada como parâmetro
     *
     * @param int $id
     *
     */
    public function delete($id)
    {
        $issetProduct = $this->getInfoWhithParm(self::$tableIntegration, 'category_id', $id);
        if ((empty($issetProduct))) {
            return $this->destroy(self::$table, $id);
        } else {
            return false;
        }
    }
}
