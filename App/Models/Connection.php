<?php

namespace App\Models;

class Connection
{
    public static $instance;

    /**
     * Realiza a conexão com o Banco de Dados
     *
     * @return object
     */
    public static function connect()
    {
        if (!isset(self::$instance)) {
            self::$instance = new \PDO(
                "mysql:host=" .  HOST . ";dbname=" . DATABASE . ";",
                USER,
                DB_PASS,
                array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8')
            );
            self::$instance->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }

        return self::$instance;
    }
}
