<?php

namespace App\Models;

use App\Models\Connection;

class Manager extends Connection
{

    public function insert($table, $data)
    {
        $pdo = parent::connect();
        $fields = implode(", ", array_keys($data));
        $values = ":" . implode(", :", array_keys($data));
        $sql = "INSERT INTO $table ($fields) VALUES($values)";
        $statement = $pdo->prepare($sql);
        foreach ($data as $key => $value) {
            $statement->bindValue(":$key", $value, \PDO::PARAM_STR);
        }
        $statement->execute();
    }

    public function verifyExists($table, $parm, $value)
    {
        $pdo = parent::connect();
        $sql = "SELECT * FROM $table WHERE $parm = ?";
        $statement = $pdo->prepare($sql);
        $statement->execute(array($value));
        $response = $statement->fetch();
        return @$response['id'];
    }

    public function list($table, $order = null)
    {
        $pdo = parent::connect();
        if ($order) {
            $sql = "SELECT * FROM $table ORDER BY $order ASC";
        } else {
            $sql = "SELECT * FROM $table";
        }

        $statement = $pdo->query($sql);
        $statement->execute();

        return $statement->fetchAll();
    }

    public function destroy($table, $id)
    {
        $pdo = parent::connect();
        $sql = "DELETE FROM $table WHERE id = :id";
        $statement = $pdo->prepare($sql);
        $statement->bindValue(':id', $id);
        return $statement->execute();
    }

    public function destroyWithParm($table, $parm, $value)
    {
        $pdo = parent::connect();
        $sql = "DELETE FROM $table WHERE $parm = :parm";
        $statement = $pdo->prepare($sql);
        $statement->bindValue(':parm', $value);
        return $statement->execute();
    }

    public function getInfo($table, $id)
    {
        $pdo = parent::connect();
        $sql = "SELECT * FROM $table WHERE id = :id";
        $statement = $pdo->prepare($sql);
        $statement->bindValue(':id', $id);
        $statement->execute();
        return $statement->fetch();
    }

    public function getInfoWhithParm($table, $parm, $value)
    {
        $pdo = parent::connect();
        $sql = "SELECT * FROM $table WHERE $parm = :parm";
        $statement = $pdo->prepare($sql);
        $statement->bindValue(':parm', $value);
        $statement->execute();
        return $statement->fetchAll();
    }

    public function updateM($table, $data, $id)
    {
        $pdo = parent::connect();
        $new_values = "";
        foreach ($data as $key => $value) {
            $new_values .= "$key=:$key, ";
        }
        $new_values = substr($new_values, 0, -2);
        $sql = "UPDATE $table SET $new_values WHERE id = $id";
        $statement = $pdo->prepare($sql);
        foreach ($data as $key => $value) {
            $statement->bindValue(":$key", $value, \PDO::PARAM_STR);
        }
        $statement->execute();
    }
}
