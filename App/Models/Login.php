<?php

namespace App\Models;

use App\Models\Connection;
use Bcrypt\Bcrypt;

class Login extends Connection
{

    /**
     * Verifica se usuário e senha existem e se estão corretos;
     * Cria a sessão 
     *
     * @param string $email
     * @param string $pass
     * @return void
     */
    public function login($email, $pass)
    {
        $pdo = parent::connect();
        $sql = "SELECT * FROM `tb_users` WHERE email = ?";
        $user = $pdo->prepare($sql);
        $user->execute(array($email));
        $user = $user->fetch();
        $bcrypt = new Bcrypt();
        if (empty($user)) {
            return false;
        } elseif ($bcrypt->verify($pass, $user['password'])) {
            $_SESSION['login'] = $user['email'];
            $_SESSION['name'] = $user['name'];
            return true;
        }
    }
}
