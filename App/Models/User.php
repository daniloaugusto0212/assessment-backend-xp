<?php

namespace App\Models;

class User extends Manager
{
    public $name;
    public $email;
    public $password;
    public static $table = 'tb_users';
    public static $parmVerify = 'email';

    /**
     * Salva Usuário no banco de dados
     *
     *
     */
    public function save()
    {
        $data = array('name' => $this->name, 'email' => $this->email, 'password' => $this->password);

        $exists = $this->verifyExists(self::$table, self::$parmVerify, $data[self::$parmVerify]);
        if ($exists != null) {
            return false;
        } else {
            $this->insert(self::$table, $data);
            return true;
        }
    }

    /**
     * Retorna a lista dos usuários
     *
     *
     */
    public function show()
    {
        return $this->list(self::$table);
    }

    /**
     * Retorna o usuário passado como parâmetro
     *
     *
     */
    public function find($id)
    {
        return $this->getInfo(self::$table, $id);
    }

    /**
     * Atualiza o usuário passado como parâmetro
     *
     * @return boolean
     */
    public function update($id)
    {
        $data = array('name' => $this->name, 'email' => $this->email);

        $this->updateM(self::$table, $data, $id);
        return true;
    }

    /**
     * Exclui o usuário passado como parâmetro
     *
     * @return boolean
     */
    public function delete($id)
    {
        return $this->destroy(self::$table, $id);
    }
}
