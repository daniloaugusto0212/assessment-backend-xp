<?php

namespace App\Models;

use App\Controllers\classes\Utils;

class Product extends Manager
{
    public $category_id;
    public $sku;
    public $name;
    public $price;
    public $quantity;
    public $description;
    public $image;
    public static $table = 'tb_products';
    public static $tableIntegration = 'tb_product_category';
    public static $tableCategory = 'tb_categories';
    public static $parmVerify = 'name';

    /**
     * Salva Produto no banco de dados
     *
     *
     */
    public function save()
    {
        $data = array(
            'sku' => $this->sku,
            'name' => $this->name,
            'price' => $this->price,
            'quantity' => $this->quantity,
            'description' => $this->description,
            'image' => $this->image
        );

        $exists = $this->verifyExists(self::$table, self::$parmVerify, $data[self::$parmVerify]);
        if ($exists != null) {
            return false;
        } else {
            $this->insert(self::$table, $data);

            /**
             * Busca id do produto inserido e faz inserção na tabela de relações
             */
            $id = $this->verifyExists(self::$table, self::$parmVerify, $data[self::$parmVerify]);
            $product_id = $this->find($id)['id'];

            //Faz um looping de acordo com a quantidade de categorias seleciondas.
            foreach ($this->category_id as $category_id) {
                $data_integration = array(
                    'category_id' => $category_id,
                    'product_id' => $product_id
                );
                $this->insert(self::$tableIntegration, $data_integration);
            }
            return true;
        }
    }

    /**
     * Retorna a lista dos produtos
     *
     *
     */
    public function show()
    {
        return $this->list(self::$table);
    }

    /**
     * Retorna a relação entre produtos e categorias
     *
     * @param int $id
     *
     */
    public function showCategories($id = null)
    {
        $tableP = self::$table;
        $tableProdId = $tableP . '.id';
        $tableRelations = self::$tableIntegration;
        $tableRelationsProdId = $tableRelations . '.product_id';
        $tableCat = self::$tableCategory;
        $tableCatName = $tableCat . '.name';
        $tableCatId = $tableCat . '.id';
        $tableRelationsCatId = $tableRelations . '.category_id';
        $where = $id == null ? '' : ' WHERE ' . $tableProdId . ' = ' . $id;
        $pdo = parent::connect();
        $sql = "SELECT $tableCatName, $tableProdId, $tableRelationsCatId FROM $tableCat
        INNER JOIN $tableRelations ON ($tableCatId = $tableRelationsCatId)
        INNER JOIN $tableP ON ($tableProdId = $tableRelationsProdId) $where";
        $statement = $pdo->prepare($sql);
        $statement->execute();
        return $statement->fetchAll();
    }

    /**
     * Busca o produto passado como parâmetro
     *
     * @param int $id
     *
     */
    public function find($id)
    {
        return $this->getInfo(self::$table, $id);
    }

    /**
     * Atualiza o produto passado como parâmetro
     *
     * @param int $id
     * @return boolean
     */
    public function update($id)
    {
        $data = array(
            'sku' => $this->sku,
            'name' => $this->name,
            'price' => $this->price,
            'quantity' => $this->quantity,
            'description' => $this->description,
            'image' => $this->image
        );
        $this->destroyWithParm(self::$tableIntegration, 'product_id', $id);

        //Faz um looping de acordo com a quantidade de categorias seleciondas.
        foreach ($this->category_id as $category_id) {
            $data_integration = array(
                'product_id' => $id,
                'category_id' => $category_id
            );
            $this->insert(self::$tableIntegration, $data_integration);
        }

        $this->updateM(self::$table, $data, $id);
        return true;
    }

    /**
     * Exclui o produto passado como parâmetro
     *
     * @param int $id
     * @return boolean
     */
    public function delete($id)
    {
        Utils::deleteFile($this->find($id)['image']);
        $deleteIntegration = $this->destroyWithParm(self::$tableIntegration, 'product_id', $id);
        if ($deleteIntegration) {
            return $this->destroy(self::$table, $id);
        } else {
            return false;
        }
    }
}
