<?php

include(__DIR__ . '/config.php');

use App\Application;
use App\Controllers\classes\Utils;

include(VIEW_PATH . '/partials/header.php');

$app = new Application();

if (isset($_GET['logout'])) {
    Utils::logout();
}

$app->run();


include(VIEW_PATH . '/partials/footer.php');
