<?php

session_start();

require('vendor/autoload.php');

const PATH = 'http://localhost/crud_produtos';

const ASSET_PATH = PATH . '/assets';

const VIEW_PATH = 'App/Views';

const DIR_UPLOAD = __DIR__ . '/uploads/';

/*Consts DATABASE */
const HOST = 'localhost';
const DATABASE = 'crud_produtos';
const USER = 'root';
const DB_PASS = '';

$id = explode('/', @$_GET['url']);

define('ID', @$id[2]);
