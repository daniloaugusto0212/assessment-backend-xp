# Orientações para rodar o projeto

- necessário servidor Apache e MySql
- importar a base de dados
- No arquivo **config.php**, setar as Constantes de **DATABASE** e a **PATH** com os seus respectivos dados 
- rodar o comando **composer install** para a instalação das dependências

Será necessário logar no sistema. Existe um único usuário cadastrado na base, o login e senha são:

- login: **daniloaugusto0212@hotmail.com**
- senha: **123456**


# Tecnologias, dependências e padrões

- PHP 7.4
- Composer
- psr-4
- padrão MVC
- Bcrypt (criptogafia)
- jquery 3.6
- jqueryMaskMoney (Máscara de preço)
- jquerySelect2 (Seleção múltipla)


