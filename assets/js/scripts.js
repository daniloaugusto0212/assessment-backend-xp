//Personalização de select múltiplo
$("#category").select2();

//Máscara de preço
$('[name=price]').maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});